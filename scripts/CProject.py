from __future__ import print_function

import os
import sys
import errno
import shutil
import argparse


verbose = False

def vprint(string):
    if verbose is True:
        print(string)

def perror(string):
    print(string, file=sys.stderr)

def createDirectory(dirName):
    if os.path.exists(dirName) is False:
        try:
            os.makedirs(dirName)
        except OSError as err:
            if err.errno != errno.EEXIST:
                perror('Error creating directory "' + dirName + '": ' + err.strerror)
                exit(-1)
    else:
        if os.path.isdir(dirName) is False:
            try:
                os.makedirs(dirName)
            except OSError as err:
                if err.errno != errno.EEXIST:
                    perror('Error creating directory "' + dirName + '": ' + err.strerror)
                    exit(-1)


def main(argv):
    global verbose

    sourcePath = argv[1]

    parser = argparse.ArgumentParser(prog="CProject")
    parser.add_argument("PROJPATH", action="store", help="Path to the project to be created.")
    parser.add_argument("-p", "--projName", action="store", dest="projName", help="The name of the project to be created. If this option is specified the project name will be appended to the project path. Default value is the basename of the project path.")
    parser.add_argument("-s", "--solName", action="store", dest="solName", help="The name of the executable. Default value is the basename of the project path.")
    parser.add_argument("--vscode", action="store_true", help="Add files specific to the Visual Studio Code project setup.")
    parser.add_argument("--includes", action="store_true", help="Add common include files to the new project.")
    parser.add_argument("-v", "--verbose", action="store_true", help="Displays aditional information while creating the new project.")

    args = parser.parse_args(argv[2:])

    if args.verbose is True:
        verbose = True

    args.PROJPATH = os.path.abspath(args.PROJPATH)

    if args.projName is None:
        args.projName = os.path.basename(args.PROJPATH)
    else:
        args.PROJPATH = os.path.join(args.PROJPATH, args.projName)

    if args.solName is None:
        args.solName = os.path.basename(args.PROJPATH)

    # If verbose is active print arguments
    vprint('Project path: "' + str(args.PROJPATH) + '"')
    vprint('Project name: "' + str(args.projName) + '"')
    vprint('Solution name: "' + str(args.solName) + '"')
    vprint('Include vscode files: "' + str(args.vscode) + '"')
    vprint('Include common header files: "' + str(args.includes) + '"\n')

    vprint('Create directory structure ...')
    createDirectory(args.PROJPATH)
    createDirectory(os.path.join(args.PROJPATH, 'src'))
    createDirectory(os.path.join(args.PROJPATH, 'include'))
    createDirectory(os.path.join(args.PROJPATH, 'bin'))
    vprint('Directory structure done!')

    vprint('Copying makefile ...')
    path_to_makefile = os.path.join(sourcePath, "Makefiles", "Makefile")
    orig_makefile = open(path_to_makefile, 'r')
    
    first_line = orig_makefile.readline()
    if first_line.startswith('PROJECT_NAME = ') is False:
        perror('Unrecognised source makefile!')
        exit(-2)

    path_to_dest_makefile = os.path.join(args.PROJPATH, 'Makefile')
    dest_makefile = open(path_to_dest_makefile, 'w')

    dest_makefile.write('PROJECT_NAME = ' + args.solName + '\n')
    for line in orig_makefile:
        dest_makefile.write(line)

    orig_makefile.close()
    dest_makefile.close()

    if args.includes is True:
        vprint('Copying include files ...')
        source_include_path = os.path.join(sourcePath, 'includes')
        dest_include_path = os.path.join(args.PROJPATH, 'include')
        include_files = os.listdir(source_include_path)
        for filename in include_files:
            include_file_path = os.path.join(source_include_path, filename)
            if os.path.isfile(include_file_path) is True:
                shutil.copy(include_file_path, dest_include_path)


    vprint('Creating initial source file ...')
    path_to_source_file = os.path.join(args.PROJPATH, 'src', args.solName + '.cpp')
    init_source_file = open(path_to_source_file, 'w')

    if args.includes is True:
        init_source_file.write('#include "dd_common_definitions.h"\n')

    init_source_file.write('#include <stdio.h>\n')
    init_source_file.write('\nint main()\n')
    init_source_file.write('{\n')
    init_source_file.write('\tprintf("Hello world!\\n");\n')
    init_source_file.write('\treturn 0;\n')
    init_source_file.write('}\n')

    init_source_file.close()

    if args.vscode is True:
        vprint('Copying .vscode files ...')

        vscode_dir = os.path.join(args.PROJPATH, '.vscode')
        createDirectory(vscode_dir)

        vscode_source_dir = os.path.join(sourcePath, 'vscodeFiles')

        task_file_path = os.path.join(vscode_source_dir, 'tasks.json')
        shutil.copy(task_file_path, vscode_dir)

        launch_source_file_path = os.path.join(vscode_source_dir, 'launch.json')
        launch_source_file = open(launch_source_file_path, 'r')

        lounch_file_path = os.path.join(vscode_dir, 'launch.json')
        lounch_file = open(lounch_file_path, 'w')

        for line in launch_source_file:
            if '||solutionname||' in line:
                line = line.replace('||solutionname||', args.solName)

            lounch_file.write(line)

        launch_source_file.close()
        lounch_file.close()


    





if __name__ == '__main__':
    main(sys.argv)