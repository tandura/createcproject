#!/bin/bash

if [ "$1" = "install" ]
then
    if [ "$(id -u)" -ne 0 ]; then
        echo 'Install must be run by root' >&2
        exit 1
    fi

    echo "Install started!"
    set -e
    ln -s $PWD/CProject.sh /usr/bin/CProject
    echo "Install finished!"
else
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
    SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
    #echo $DIR

    python3 $SCRIPT_DIR/scripts/CProject.py $SCRIPT_DIR $@
fi
