#ifndef __DD_COMMON_DEFINITIONS_H__
#define __DD_COMMON_DEFINITIONS_H__

/* 
    Detect operating system and nr. bits.
    Supported: Windows + Visual Studio; Linux + gcc
*/
#if defined(__GNUC__)
#   define DD_LINUX
#   if __x86_64__ || __ppc64__
#       define DD_64BIT
#   else
#       define DD_32BIT
#   endif
#   if defined(__APPLE__) || defined(__MACH__)
#       define DD_OSX
#   endif
#elif defined(_MSC_VER)
#   define DD_WINDOWS
#   if _WIN64
#       define DD_64BIT
#   else
#       define DD_32BIT
#   endif
#endif

/* Basic constants */
#ifndef NULL
#   define NULL 0
#endif

#ifndef FALSE
#   define FALSE 0
#endif

#ifndef TRUE
#   define TRUE 1
#endif

#ifdef DD_LINUX
#   define GCC_MAY_ALIAS __attribute__((__may_alias__))
#else
#   define GCC_MAY_ALIAS
#endif

/* Portable data types */
#ifdef __cplusplus
    typedef bool                Bool;
#else
    typedef int                 Bool;
#endif
typedef char                    Int8;
typedef unsigned char           UInt8;
typedef short                   Int16;
typedef unsigned short          UInt16;
typedef int                     Int32;
typedef unsigned int            UInt32 GCC_MAY_ALIAS;

#if defined(DD_WINDOWS)
    typedef __int64             Int64;
    typedef unsigned __int64    UInt64;
#   define EXPORT __declspec(dllexport)
#elif defined(DD_LINUX)
    typedef long long           Int64;
    typedef unsigned long long  UInt64;
#   define EXPORT __attribute__((visibility("default")))
#endif

#if defined(DD_32BIT)
    typedef UInt32 Size_t;
#elif defined(DD_64BIT)
    typedef UInt64 Size_t;
#endif

#ifdef DD_DEBUG
#   define dbgprintf(...) printf(__VA_ARGS__);
#else
#   define dbgprintf(...) ;
#endif

#define CHECKRET(x,retValue)  { if (!(x)) { \
                                dbgprintf("[EXCEPTION] %s line:%4d (%s) -> %s\n",__FILE__,__LINE__,#x,#retValue); \
                                return (retValue); } }

#define CHECKBRK(x)  { if (!(x)) { \
                                dbgprintf("[EXCEPTION] %s line:%4d (%s)\n",__FILE__,__LINE__,#x); \
                                break; } }

#endif