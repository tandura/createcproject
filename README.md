# CProject

CProject it's a C/C++ project creator. It creates a default project structure, creates a makefile to build the project, and if specified can add to it common header files and setup files for VS Code.

---

## Instalation

* Installing prerequisite packages:
    * To check if the **python** is installed run the following command in a terminal: `python --version` (tested for 2.7+ and 3.6+)
    * If the python is not installed run in a terminal one of the following: `sudo apt-get install python` or `sudo apt install python`
    * To check if **git** is installed run the following command in a terminal: `git --version`
    * If git is not installed run in a terminal one of the following: `sudo apt-get install git` or `sudo apt install git`
    * For building C/C++ project via makefiles we nead the **make** command. To check if make is installed run in a terminal: `make --version`
    * To install the make and/or the gcc/g++ compilers run in terminal one of the following: `sudo apt-get install build-essential` or `sudo apt install build-essential`
    * To build for x86 architecture we nead the following packages: `sudo apt-get install gcc-multilib g++-multilib` or `sudo apt install gcc-multilib g++-multilib`
* Cloning the **createcproject** repository:
    * Open a terminal and move to the directory in which you want to clone the project
    * Run the following command: `git clone https://bitbucket.org/tandura/createcproject.git`
* Install the **CProject**:
    * Change directory to the cloned project by running: `cd createcproject`
    * Make sure that the **CProject.sh** has execution permisions by running: `chmod 0764 CProject.sh`
    * Install the CProject by running: `sudo ./CProject.sh install`
* Oprional: Install **VS Code**
    * To install VS Code on linux follow [this](https://code.visualstudio.com/docs/setup/linux) instructions.

---

## Usage

To view the usage of CProject run un one of the following commands in a terminal: `CProject -h` or `CProject --help`